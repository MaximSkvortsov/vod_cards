#include "filmsmodel.h"

FilmsModel::FilmsModel(QObject *parent): QAbstractListModel(parent)
{

}

QVariant FilmsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int id = index.row();
    switch (role) {
    case TitleRole:
        return mData.at(id).name();
    case OriginalNameRole:
        return mData.at(id).originalName();
    case CountryRole:
        return mData.at(id).country();
    case DescriptionRole:
        return mData.at(id).description();
    case DirectorRole:
        return mData.at(id).director();
    case DurationRole:
        return mData.at(id).duration();
    case ActorsRole:
        return mData.at(id).actors();
    case YearRole:
        return mData.at(id).year();
    case GenresRole: {
        QList<QVariant> genres;
        for (const int& element: mData.at(id).genres()) {
            genres.append(QVariant(element));
        }
        return genres;
    }
    case GroupsRole: {
        QList<QVariant> groups;
        for (const int& element: mData.at(id).groups()) {
            groups.append(QVariant(element));
        }
        return groups;
    }
    };

    return QVariant();
}

int FilmsModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return mData.size();
}

QHash<int, QByteArray> FilmsModel::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
    roles[TitleRole] = "title";
    roles[OriginalNameRole] = "originalTitle";
    roles[CountryRole] = "country";
    roles[DescriptionRole] = "description";
    roles[DirectorRole] = "director";
    roles[DurationRole] = "duration";
    roles[ActorsRole] = "actors";
    roles[YearRole] = "year";
    roles[GenresRole] = "genres";
    roles[GroupsRole] = "groups";

    return roles;
}

void FilmsModel::fromJson(const QUrl &path)
{
    if (mData.size() > 0) {
        beginRemoveRows(QModelIndex(), 0, mData.size());
        mData.clear();
        endRemoveRows();
    }
    JsonReader& reader = JsonReader::getInstance();
    try {
        reader.setPath(path);
    } catch (std::invalid_argument& err) {
        emit error(QString(err.what()));
        return;
    } catch (std::runtime_error& err) {
        emit error(QString(err.what()));
        return;
    }
    QJsonObject jsonObject = reader.data();
    QVariantMap jsonMap = jsonObject.toVariantMap();
    QVariantMap contentMap = jsonMap["contentByGroupsAndGenres"].toMap();
    QVariantList films = contentMap["films"].toList();
    if (films.size() > 0) {
        beginInsertRows(QModelIndex(), 0, films.size() - 1);
        for (const QVariant& element: films) {
            mData.append(Film());
            mData.last().fromJSON(element.toMap());
        }
        endInsertRows();
    }

}
