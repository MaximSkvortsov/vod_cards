import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtLocation 5.9

Item {
    id: root
    width: defaultWidth
    height: defaultHeight

    property int defaultWidth
    property int defaultHeight: {
        return defaultWidth*9/16 + extendedTitle.height  - description.height - radius*2;
    }

    property alias title: title.text
    property alias description: description.text
    property alias posterSource: poster.source
    property alias extendedInfo: extendedInfo.text
    property bool selected
    property int animationsDuration: 500
    property int radius: 2

    signal clicked(var mouse)

    ColumnLayout {
        anchors.fill: parent
        z: root.z + 2
        Image {
            id: poster
            Layout.alignment: Qt.AlignTop
            Layout.topMargin: extendedTitle.height - 5
            asynchronous: true
            fillMode: Image.PreserveAspectCrop
            source: "qrc:/resources/poster_placeholder.jpg"
            sourceSize {
                width: root.width
                height: width*9/16
            }

            Rectangle {
                id: posterTitleBackground
                width: parent.width
                height: 50
                z: parent.z + 1
                opacity: 0.35
                color: "black"
                anchors.bottom: parent.bottom
            }

            Text {
                id: posterTitle
                z: posterTitleBackground.z + 1
                text: title.text

                anchors {
                    right: parent.right
                    left: parent.left
                    verticalCenter: posterTitleBackground.verticalCenter
                    leftMargin: 10
                    rightMargin: 10
                }
                wrapMode: Text.Wrap
                elide: Text.ElideRight
                font{
                    family: "Arial"
                    pixelSize: 14
                }
                color: "white"
            }

            MouseArea {
                id: mainMouseArea
                anchors {
                    fill: parent
                }

                onClicked: {
                    root.clicked(mouse);
                }
            }
        }
    }


    Rectangle {
        id: extendedTitle
        y: poster.y + 3
        z: poster.z - 1
        radius: root.radius
        height: 45
        width: root.width - 3
        color: "white"

        Text {
            id: title
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                leftMargin: 10
                topMargin: 5
            }
            width: parent.width - 20
            color: "black"
            elide: Text.ElideRight
            font {
                family: "Arial"
                pixelSize: 14
            }
        }

        Text {
            id: extendedInfo
            anchors {
                top: title.bottom
                left: parent.left
                right: parent.right
                leftMargin: 10
            }
            width: parent.width - 20
            color: "black"
            elide: Text.ElideRight
            font {
                family: "Arial"
                pixelSize: title.font.pixelSize - 2
            }
        }


        Behavior on y {
            PropertyAnimation{
                duration: root.animationsDuration
            }
        }
    }

    Rectangle {
        id: descriptionBackground
        y: poster.y + 3
        z: poster.z - 1
        height: {
            var maxValue = poster.height - 3;
            var preffered = root.height - extendedTitle.height - root.radius - maxValue;
            return preffered > maxValue ? maxValue : preffered;
        }
        width: root.width - 3
        radius: root.radius

        Text {
            id: description
            anchors {
                fill: parent
                margins: 10
                topMargin: 10 + radius
                bottomMargin: actionsButton.height + 5
            }

            wrapMode: Text.Wrap
            elide: Text.ElideRight
            height: descriptionBackground.height - actionsButton.height - Layout.topMargin - Layout.bottomMargin
        }

        Rectangle {
            id: actionsButton
            height: 20
            width: height
            radius: height
            z: actionsArea.z + 1
            anchors {
                bottom: parent.bottom
                right: parent.right
                margins: 5
            }

            color: "#3ad7fc"

            Text {
                id: actionsButtonText
                anchors.centerIn: parent

                text: "i"
                color: "white"
                font{
                    family: "Arial"
                    pointSize: 12
                }

                states: [
                    State {
                        name: "toggled"
                        PropertyChanges{target: actionsButtonText; text: ">"}
                    }
                ]
            }

            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton
                onClicked: {
                    actionsButton.toggle();
                }
            }

            function toggle() {
                if (!actionsAreaWidthAnimation.running &&
                        !actionsAreaHeightAnimation.running &&
                        !actionsAreaRadiusAnimation.running) {
                    actionsArea.state = actionsArea.state == "" ? "unfolded" : "";
                    actionsButtonText.state = actionsButtonText.state == "" ? "toggled" : "";
                    root.focus = !root.focus;
                    if (!root.focus)
                        actionsRepeater.itemAt(0).forceActiveFocus();
                    else
                        root.forceActiveFocus();
                }
            }
        }

        Rectangle {
            id: actionsArea
            anchors {
                bottom: parent.bottom
                right: parent.right
                margins: 10
            }
            z: descriptionBackground.z + 1
            width: 0
            height: 0
            radius: 20
            color: "#3ad7fc"

            Repeater {
                id: actionsRepeater
                anchors.fill: parent
                model: 3
                delegate:
                    Button {
                    id: button
                    height: 20
                    visible: (actionsArea.width > actionsButton.width) && !actionsAreaWidthAnimation.running
                    anchors {
                        left: parent.left
                        right: parent.right
                        top: parent.top
                        topMargin: height*index + root.radius + 5
                    }
                    flat: true

                    focus: true
                    hoverEnabled: true

                    text: "Button for some action"

                    font {
                        family: "Arial"
                        pixelSize: 12
                    }

                    background: Rectangle {
                        anchors.fill: parent
                        opacity: button.hovered || button.down ? 0.3 : 0
                        color: button.down ? "#147cfc" : "#14acfc"
                    }

                    contentItem: Text {
                        text: button.text
                        font: button.font
                        color: "white"
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        elide: Text.ElideRight
                    }

                    KeyNavigation.down: actionsRepeater.itemAt(index + 1)
                    KeyNavigation.up: actionsRepeater.itemAt(index - 1)
                }
            }

            Behavior on height {
                PropertyAnimation {
                    id: actionsAreaHeightAnimation
                    duration: root.animationsDuration
                }
            }
            Behavior on width {
                PropertyAnimation {
                    id: actionsAreaWidthAnimation
                    duration: root.animationsDuration
                }
            }
            Behavior on radius {
                PropertyAnimation {
                    id: actionsAreaRadiusAnimation
                    duration: root.animationsDuration
                }
            }

            states: [
                State {
                    name: "unfolded"
                    PropertyChanges{target: actionsArea; radius: descriptionBackground.radius}
                    PropertyChanges{target: actionsArea; width: descriptionBackground.width}
                    PropertyChanges{target: actionsArea; height: descriptionBackground.height}
                    PropertyChanges{target: actionsArea; anchors.margins: 0}
                }
            ]
        }

        Behavior on y {
            PropertyAnimation{
                duration: root.animationsDuration
            }
        }

    }

    Behavior on scale {
        PropertyAnimation {
            duration: root.animationsDuration
        }
    }

    function toggleActionsArea() {
        actionsButton.toggle();
    }

    states: [
        State {
            name: "selected"
            PropertyChanges{target: root; z: 10}
            PropertyChanges{target: root; scale: root.parent.selectionScaleRatio}
            PropertyChanges{target: extendedTitle; y: 0}
            PropertyChanges{target: descriptionBackground; y: poster.y + descriptionBackground.height - 5}
            PropertyChanges{target: extendedTitle; width: root.width}
            PropertyChanges{target: descriptionBackground; width: root.width}
            PropertyChanges{target: posterTitleBackground; height: 0}
            PropertyChanges{target: posterTitle; text: ""}
            PropertyChanges{target: actionsButton; z: root.z + 1}
        }
    ]

    Behavior on height {
        PropertyAnimation{
            duration: root.animationsDuration
        }
    }

    onSelectedChanged: {
        state = selected ? "selected" : "";
        actionsButton.state = "";
        actionsArea.state = "";
    }
}
