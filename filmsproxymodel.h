#ifndef FILMSPROXYMODEL_H
#define FILMSPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <QSet>

#include "filmsmodel.h"

class FilmsProxyModel: public QSortFilterProxyModel
{
    Q_OBJECT
public:
    FilmsProxyModel(QObject* parent = 0);
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;

    Q_INVOKABLE void setFilterValues(const QVariantList &values);

    QUrl path() {return mPath;}
    void setPath(const QUrl& path);

    Q_PROPERTY(QUrl path READ path WRITE setPath NOTIFY pathChanged)

private:
    QList<int> mFilterValues;
    FilmsModel mFilms;
    QUrl mPath;

signals:
    void pathChanged();
    void error(const QString& message);
};

#endif // FILMSPROXYMODEL_H
