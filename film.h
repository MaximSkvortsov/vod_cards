#ifndef FILM_H
#define FILM_H

#include <QUrl>
#include <QString>
#include <QHostAddress>

// Container for some data about Film

struct Film
{
public:
    Film();
    ~Film(){}

    void fromJSON(QVariantMap jsonMap);

    QString country() const {return mCountry;}
    QString description() const {return mDescription;}
    QString name() const {return mName;}
    QString originalName() const {return mOriginalName;}
    QString actors() const {return mActors;}
    QString director() const {return mDirector;}
    QUrl url() const {return mUrl;}
    QUrl trailerUrl() const {return mTrailerUrl;}

    QList<int> genres() const {return mGenres;}
    QList<int> groups() const {return mGroups;}

    unsigned short int year() const {return mYear;}
    unsigned short int duration() const {return mDuration;}
    unsigned short int durationSec() const {return mDurationSec;}
    unsigned int id() const {return mId;}

private:
    QString mCountry;
    QString mDescription;
    QString mName;
    QString mOriginalName;
    QString mActors;
    QString mDirector;
    QUrl mUrl;
    QUrl mTrailerUrl;

    QList<int> mGenres;
    QList<int> mGroups;

    unsigned short int mYear;
    unsigned short int mDuration;
    unsigned short int mDurationSec;
    unsigned int mId;
};

#endif // FILM_H
