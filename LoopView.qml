import QtQuick 2.0

/* Provides path where components are looped.
  Number of components on the front line is based on delegateWidth and spacing.
  All other components, which are not on the front line, are moving around out of the screen -
  at y = -1000
*/
PathView {
    id: root

    highlightMoveDuration: 1000
    property int delegateWidth
    property int topMargin: 0
    property int spacing: 0
    property real selectionScaleRatio

    cacheItemCount: 2*path.delegatesInFront

    path: Path {
        id: path

        property int selectionMargin: (root.selectionScaleRatio - 1)*root.delegateWidth/2
        property int delegateSize: root.delegateWidth + root.spacing;
        property int delegatesInFront: Math.round(((root.width - root.delegateWidth)/delegateSize))
        property real frontLinePercentage: delegatesInFront/root.count

        startX: root.x + root.delegateWidth;
        startY: root.y + root.topMargin;

        PathPercent { value: 0}
        PathLine {
            relativeX: path.delegateSize + path.selectionMargin
            relativeY: 0
        }
        PathPercent { value: 1/root.count}
        PathLine {
            relativeX: (path.delegatesInFront - 1) * path.delegateSize
            relativeY: 0
        }

        PathPercent { value: path.frontLinePercentage}
        PathLine {
            relativeX: path.delegateSize;
            relativeY: 0
        }
        PathPercent { value: path.frontLinePercentage + 1/root.count}
        PathLine {
            relativeX: path.delegateSize;
            y: -1000
        }
        PathPercent { value: path.frontLinePercentage + 1/root.count  + 0.0001}
        PathLine {
            x: root.x - path.delegateSize - root.spacing;
            relativeY: 0;
        }
        PathPercent { value: path.frontLinePercentage + 1/root.count + 0.0001}
        PathLine {
            relativeX: - 2*root.width;
            y: root.y + root.topMargin;
        }
        PathPercent { value: root.count > 3 ? 1 - 3/root.count : 1}
        PathLine {
            x: root.x - path.delegateSize;
            y: root.y + root.topMargin;
        }
        PathPercent { value: root.count > 2 ? 1 - 2/root.count : 1}
        PathLine {
            x: root.x - root.spacing - path.selectionMargin
            y: root.y + root.topMargin;
        }
        PathPercent { value: root.count > 1 ? 1 - 1/root.count : 1}
        PathLine {
            x: root.x + root.delegateWidth;
            relativeY: 0
        }
        PathPercent { value: 1}
    }

    Keys.onLeftPressed: decrementCurrentIndex();
    Keys.onRightPressed: incrementCurrentIndex();    
}

