#include "filmsproxymodel.h"

FilmsProxyModel::FilmsProxyModel(QObject *parent) : QSortFilterProxyModel(parent)
{
    setSourceModel(&mFilms);
    setFilterRole(FilmsModel::GroupsRole);
    setFilterKeyColumn(1);

    connect(&mFilms, &FilmsModel::error, this, &FilmsProxyModel::error);
}

bool FilmsProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    if (mFilterValues.isEmpty())
        return true;
    QModelIndex index = sourceModel()->index(source_row, 0, source_parent);
    int role = filterRole();
    QVariant data = sourceModel()->data(index, role);
    bool accepted = true;
    for (const QVariant& element: data.toList()) {
        accepted = accepted && mFilterValues.contains(element.toInt());
    }
    return accepted;
}

void FilmsProxyModel::setFilterValues(const QVariantList &values)
{
    mFilterValues.clear();
    for (const QVariant& element: values) {
        mFilterValues.append(element.toInt());
    }
    invalidateFilter();
}

void FilmsProxyModel::setPath(const QUrl &path)
{
    mPath = path;
    mFilms.fromJson(path);
    pathChanged();
}
