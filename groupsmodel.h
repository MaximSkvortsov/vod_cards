#ifndef GENREMODEL_H
#define GENREMODEL_H

#include <memory>
#include <QSortFilterProxyModel>
#include <QAbstractListModel>
#include <QString>
#include <QHash>
#include <QUrl>
#include <QMap>

#include "film.h"
#include "filmsmodel.h"
#include "filmsproxymodel.h"

class GroupsModel: public QAbstractListModel
{
    Q_OBJECT
public:

    enum Roles {
        NameRole = Qt::UserRole + 1,
        NameIndexRole,
        IconRole
    };

    GroupsModel(QObject* parent = 0);

    void fromJSON(const QUrl &path);

    QVariant data(const QModelIndex &index, int role) const;
    int rowCount(const QModelIndex &parent) const;

    QHash<int, QByteArray> roleNames() const;

    QUrl path() {return mPath;}
    void setPath(const QUrl& path);

    Q_PROPERTY(QUrl path READ path WRITE setPath NOTIFY pathChanged)

private:
    QMap<int, QString> mGenres;
    QMap<int, QString> mGroups;
    QUrl mPath;

signals:
    void error(const QString& message);
    void pathChanged();
};

#endif // GENREMODEL_H
