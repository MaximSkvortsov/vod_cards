#include "jsonreader.h"

#include <QFile>

JsonReader& JsonReader::getInstance()
{
    static JsonReader instance;
    return instance;
}

void JsonReader::setPath(const QUrl &path)
{
    if (path == mPath)
        return;
    mPath = path;
    readFile();
}

QJsonObject JsonReader::data() const
{
    return mData;
}

void JsonReader::readFile()
{
    if (mPath.isEmpty())
        return;

    QFile jsonData;
    jsonData.setFileName(mPath.path());
    if (!jsonData.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QString message = "Couldn't open JSON file: " + mPath.fileName();
        throw std::invalid_argument(message.toStdString());
    }
    QString data = jsonData.readAll();
    jsonData.close();

    QJsonParseError parseError;
    QJsonDocument jsonDocument = QJsonDocument::fromJson(data.toUtf8(), &parseError);
    if (parseError.error != QJsonParseError::NoError) {
        QString message = "Couldn't parse JSON: " + parseError.errorString();
        throw std::runtime_error(message.toStdString());
    }
    mData = jsonDocument.object();
}
