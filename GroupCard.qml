import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {
    id: root
    width: defaultWidth
    height: defaultHeight

    property int defaultWidth
    property int defaultHeight

    property alias iconPath: icon.source
    property alias title: title.text
    property int titleIndex
    property bool selected
    property bool compact
    property int animationsDuration: 500
    property int radius: 2

    signal clicked(var mouse)


    Image {
        id: poster
        width: parent.width
        height: width*9/16
        source: "qrc:/resources/group_poster_placeholder.jpg"
        fillMode: Image.PreserveAspectCrop
        anchors {
            bottom: infoRect.top
            bottomMargin: -2
            left: parent.left
            right: parent.right
        }

        Behavior on height {
            PropertyAnimation {
                duration: root.animationsDuration
            }
        }
    }

    // Contains group icon and group name
    Rectangle {
        id: infoRect
        height: 100
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        color: "#3ad7fc"

        Image {
            id: icon
            fillMode: Image.PreserveAspectCrop
            sourceSize: Qt.size(30, 30)

            anchors {
                left: parent.left
                leftMargin: 10
                verticalCenter: parent.verticalCenter
            }
        }

        Text {
            id: title
            anchors {
                verticalCenter: parent.verticalCenter
                left: icon.right
                leftMargin: 10
            }

            font{
                family: "Arial"
                pixelSize: 16
            }
            wrapMode: Text.Wrap
            elide: Text.ElideRight
            color: "black"
        }


        Behavior on height {
            PropertyAnimation {
                duration: root.animationsDuration
            }
        }
        Behavior on radius {
            PropertyAnimation {
                duration: root.animationsDuration
            }
        }
    }


    MouseArea {
        anchors.fill: parent
        onClicked: {
            parent.clicked(mouse)
        }
    }

    Behavior on scale {
        PropertyAnimation {
            duration: root.animationsDuration
        }
    }

    states: [
        State {
            name: "selected"
            PropertyChanges{target: root; z: 100}
            PropertyChanges{target: root; scale: root.parent.selectionScaleRatio}
            PropertyChanges{target: title; font.pixelSize: 18}
        },
        State {
            name: "compact"
            PropertyChanges{target: root; scale: 1}
            PropertyChanges{target: poster; height: 0}
            PropertyChanges{target: infoRect; radius: root.radius}
        },
        State {
            name: "selectedCompact"
            PropertyChanges{target: root; z: 100}
            PropertyChanges{target: root; scale: root.parent.selectionScaleRatio + 0.1}
            PropertyChanges{target: infoRect; radius: root.radius}
            PropertyChanges{target: poster; height: 0}
            PropertyChanges{target: title; font.pixelSize: 18}
        }
    ]

    onCompactChanged: {
        adjustState();
    }

    onSelectedChanged: {
        adjustState();
    }

    function adjustState() {
        if (compact) {
            state = selected ? "selectedCompact" : "compact"
        } else {
            state = selected ? "selected" : ""
        }
    }

}
