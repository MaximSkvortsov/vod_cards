import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

import FilmsProxyModel 1.0
import FilmsModel 1.0
import GroupsModel 1.0

import "."

Window {
    id: mainWindow
    visible: true
    width: 1200
    height: 700
    minimumHeight: 700
    minimumWidth: 900
    title: qsTr("VoD cards")

    GroupsModel {
        id: groupsModel
    }

    FilmsProxyModel {
        id: filmsModel
        path: groupsModel.path
    }    

    FileDialog {
        nameFilters:  ["*.json"]
        Component.onCompleted: open()
        title: "Choose .json data file"
        onAccepted: {
            var prefix = "file:///";
            groupsModel.path = fileUrl.toString().substring(prefix.length);
        }
    }

    Image {
        id: backgroundImage
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
        source: "qrc:/resources/main_background.jpg"
    }

        LoopView {
            id: filmsLoopView
            visible: false
            height: Window.height*0.65
            width: Window.width
            anchors {
                top: parent.top
                topMargin: Window.height*0.1
            }

            spacing: 5
            selectionScaleRatio: 1.2

            model: filmsModel

            delegate: FilmCard {
                defaultWidth: filmsLoopView.delegateWidth
                defaultHeight: filmsLoopView.height/filmsLoopView.selectionScaleRatio
                title: model.title
                extendedInfo: model.country + " " + model.year
                description: model.description
                selected: PathView.isCurrentItem && filmsLoopView.visible

                onClicked: {
                    if (selected)
                        parent.switchFocusToGroups();
                    else
                        parent.currentIndex = index;
                }
            }
            delegateWidth: {
                var calculated = Window.width/5;
                return calculated > 300 ? calculated : 300;
            }
            topMargin: filmsLoopView.height/2 - Window.height*0.1

            Keys.onPressed: {
                if (event.key === Qt.Key_Enter ||
                        event.key === Qt.Key_Return ||
                        event.key === Qt.Key_Select) {
                    currentItem.toggleActionsArea();
                }
                if (event.key === Qt.Key_Down ||
                        event.key === Qt.Key_Back ||
                        event.key === Qt.Key_Escape) {
                    switchFocusToGroups()
                }
            }

            function switchFocusToGroups() {
                visible = false;
                focus = false;
                groupsLoopView.focus = true;
            }
        }

        LoopView {
            id: groupsLoopView

            height: Window.height*0.25
            width: Window.width
            anchors {
                bottom: parent.bottom
                bottomMargin: 20
            }

            spacing: 10
            focus: true

            model: groupsModel

            delegate: GroupCard {
                defaultWidth: groupsLoopView.delegateWidth
                defaultHeight: 100
                anchors.bottom: parent.bottom
                title: model.name
                titleIndex: model.nameIndex
                iconPath: model.icon
                selected: PathView.isCurrentItem
                compact: !groupsLoopView.focus
                onClicked: {
                    if (selected) {
                        parent.switchFocusToFilms();
                    } else {
                        parent.currentIndex = index;
                        if (compact)
                            parent.switchFocusToFilms();
                    }
                }
            }
            delegateWidth: 300
            selectionScaleRatio: 1.1

            Keys.onPressed: {
                if (event.key === Qt.Key_Enter ||
                        event.key === Qt.Key_Return ||
                        event.key === Qt.Key_Select ||
                        event.key === Qt.Key_Up)
                    switchFocusToFilms();
            }

            function switchFocusToFilms() {
                filmsModel.setFilterValues([currentItem.titleIndex])              
                groupsLoopView.focus = false;;
                filmsLoopView.visible = true;
                filmsLoopView.focus = true;
            }
        }


}
