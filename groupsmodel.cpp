#include "groupsmodel.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include "jsonreader.h"

GroupsModel::GroupsModel(QObject *parent) : QAbstractListModel(parent)
{

}

void GroupsModel::fromJSON(const QUrl &path)
{
    mGroups.clear();
    JsonReader& reader = JsonReader::getInstance();
    try {
        reader.setPath(path);
    } catch (std::invalid_argument& err) {
        emit error(QString(err.what()));
        return;
    } catch (std::runtime_error& err) {
        emit error(QString(err.what()));
        return;
    }
    QJsonObject jsonObject = reader.data();
    QVariantMap jsonMap = jsonObject.toVariantMap();
    QVariantMap contentMap = jsonMap["contentByGroupsAndGenres"].toMap();
    QVariantList groups = contentMap["groups"].toList();

    if (groups.size() > 0) {
        beginInsertRows(QModelIndex(), 0, groups.size() - 1);
        for(const QVariant& element: groups) {
            int number = element.toInt();
            mGroups[number] = "Group " + QString::number(number);
        }
        endInsertRows();
    }
}

QVariant GroupsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    int id = index.row();
    switch (role) {
    case NameRole: {
        auto values = mGroups.values();
        return QVariant(values[id]);
    }
    case NameIndexRole: {
        QString value = mGroups.values()[id];
        return mGroups.key(value);
    }
    case IconRole:
        return "qrc:/resources/group_icon.png";
    }

    return QVariant();
}

int GroupsModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return mGroups.size();
}

QHash<int, QByteArray> GroupsModel::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
    roles[NameRole] = "name";
    roles[NameIndexRole] = "nameIndex";
    roles[IconRole] = "icon";
    return roles;
}

void GroupsModel::setPath(const QUrl &path)
{
    mPath = path;
    fromJSON(path);
    pathChanged();
}
