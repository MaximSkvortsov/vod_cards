#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "groupsmodel.h"
#include "filmsmodel.h"
#include "filmsproxymodel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<FilmsProxyModel>("FilmsProxyModel", 1, 0, "FilmsProxyModel");
    qmlRegisterType<FilmsModel>("FilmsModel", 1, 0, "FilmsModel");
    qmlRegisterType<GroupsModel>("GroupsModel", 1, 0, "GroupsModel");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;


    return app.exec();
}
