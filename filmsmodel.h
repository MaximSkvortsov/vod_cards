#ifndef CARDSMODEL_H
#define CARDSMODEL_H

#include <QUrl>
#include <QHash>
#include <QString>
#include <QAbstractListModel>

#include "film.h"
#include "jsonreader.h"

// Provides simple model for Films

class FilmsModel: public QAbstractListModel
{
    Q_OBJECT
public:

    enum Roles {
        TitleRole = Qt::UserRole + 1,
        OriginalNameRole,
        CountryRole,
        DescriptionRole,
        DirectorRole,
        DurationRole,
        ActorsRole,
        YearRole,
        GenresRole,
        GroupsRole
    };

    FilmsModel(QObject* parent = 0);

    QVariant data(const QModelIndex &index, int role) const;
    int rowCount(const QModelIndex &parent) const;
    QHash<int, QByteArray> roleNames() const;
    void fromJson(const QUrl& path);

private:
    QList<Film> mData;

signals:
    void error(const QString& message);
};

#endif // CARDSMODEL_H
