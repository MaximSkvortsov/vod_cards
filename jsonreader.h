#ifndef JSONREADER_H
#define JSONREADER_H

#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>

class JsonReader
{
public:
    static JsonReader& getInstance();
    void setPath(const QUrl& path);
    QJsonObject data() const;

private:
    JsonReader() {}
    ~JsonReader() {}
    JsonReader(const JsonReader&) = delete;
    JsonReader(JsonReader&&) = delete;
    JsonReader& operator=(const JsonReader&) = delete;
    JsonReader& operator=(JsonReader&&) = delete;

    QUrl mPath;
    QJsonObject mData;

    void readFile();
};

#endif // JSONREADER_H
