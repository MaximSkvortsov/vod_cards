#include "film.h"

Film::Film()
{

}

void Film::fromJSON(QVariantMap jsonMap)
{
    mCountry = jsonMap["country"].toString();
    mDescription  = jsonMap["description"].toString();
    mName = jsonMap["name"].toString();
    mOriginalName = jsonMap["originalName"].toString();
    mActors = jsonMap["actors"].toString();
    mDirector = jsonMap["director"].toString();
    mUrl = jsonMap["url"].toUrl();
    mTrailerUrl = jsonMap[""].toUrl();

    mYear = jsonMap["year"].toUInt();
    mDuration = jsonMap["duration"].toUInt();
    mDurationSec = jsonMap["duration_sec"].toUInt();

    mId = jsonMap["id"].toUInt();

    for (const auto& element: jsonMap["genre"].toList()){
        mGenres.append(element.toInt());
    }
    for (const auto& element: jsonMap["group"].toList()){
        mGroups.append(element.toInt());
    }
}
